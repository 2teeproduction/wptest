### CREATE DATABASE ###
`docker exec -i wp-mariadb /usr/bin/mysql -u root --password=123456 -e "create database db_tm;"`

### RUN DATABASE MIGRATION ###
`cat sql/db_tm.sql | docker exec -i wp-mariadb /usr/bin/mysql -u root --password=123456 db_tm`
