<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_tm' );

/** MySQL database username */
define( 'DB_USER', 'tm' );

/** MySQL database password */
define( 'DB_PASSWORD', 'tm1234' );

/** MySQL hostname */
define( 'DB_HOST', 'wp-mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~h}|yN`:wv!~onrh9T-2B!@vF,$R+(m`z.RBaYiBR{vyk2~tf#K@@].vqf/AD>|%' );
define( 'SECURE_AUTH_KEY',  '3M>VV#k.59y;Wr;e)ZQM(j{BV4M^[-y1~dI4j];@W[?k>L4Pt<|4?Ca/@N>Aj X{' );
define( 'LOGGED_IN_KEY',    'gklDfX-F i*OKDS.HDCMB<(5).rFsn,Vhq/ku~%wM^`_5$47*l~Itv%ooy=?&q#+' );
define( 'NONCE_KEY',        'sM_Y|m!7j>fu,ZK8jbS4ah=Q~bWJ!afXX0VX:2X^@42HkIr1##_2{Z,:}6J+s.s@' );
define( 'AUTH_SALT',        '.v-FHM+~rNve5bSc?%wzPtA~cV>lkfIp8@]aW&_)S=uH()p<fZl0~O^tBMA^g8w>' );
define( 'SECURE_AUTH_SALT', 'pr+&^!h^H(G5h/m$}-bkR/_(+Du$-z`LO$:m9AJ0EyrHpkrRb!|6 H9o0xz5-D{Q' );
define( 'LOGGED_IN_SALT',   '6l!8vcs9*7%|lq?H*7;w:196jn(UFz<[&oGTrGKxkb:a24MGi~>^kiWC}kKv11P2' );
define( 'NONCE_SALT',       'P{7x+-2p-DCL~iS{ZV5h/kT1r4YK8jv;!L6|.BG_D=G8Hh)%Gi0S  >(a;q7Zp+a' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

